package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/julienschmidt/httprouter"
)

var config struct {
	AdaptorUrl string
	ServerUrl  string
}

func init() {
	config.AdaptorUrl = "http://localhost:8000"
	config.ServerUrl = "localhost:9000"
}

// Handler for sending SLA content
func XmlHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) { //XML handler for testing step 2
	fmt.Println("\nAction: Sending XML file for SLA ID =", ps.ByName("sla_id"))
	xmlFile, err := os.Open("sla.xml")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer xmlFile.Close()
	xmlContent, _ := ioutil.ReadAll(xmlFile)
	w.Write(xmlContent)
}

func main() {
	fmt.Println("\n\nStart server-imitator on", config.ServerUrl)
	go func() { //here starts goroutine for listening to requests
		router := httprouter.New()
		router.GET("/cloud-sla/slas/:sla_id", XmlHandler)

		err := http.ListenAndServe(config.ServerUrl, router) //star web server
		if err != nil {
			fmt.Println(err)
		}
	}()

	// Instructions for console commands
	fmt.Println("Avaliable commands:\n")
	fmt.Println("1) \"new sla {:sla-id}\"\nDesc: e.g. type \"new sla 123\" for PUT request to /adaptor/sla-creatiion/123\n")
    fmt.Println("2) \"send msg {:label1 :label2 ...}\"\nDesc: PUT request to /adaptor/event with fake Message (correct JSON object)\n")
    fmt.Println("3) \"send cor msg {:label1 :label2 ...}\"\nDesc: PUT request to /adaptor/event with corrupted JSON object\n")
	fmt.Println("4) \"del sla {:sla-id}\"\nDesc: e.g. type \"del sla 123\" for PUT request to /adaptor/sla-termination/123\n")
	fmt.Println("5) \"exit\"\nDesc: Close programm\n")
	fmt.Print("Enter command: ")

	// Read commands from console
	commandChan := make(chan string)
	scanner := bufio.NewScanner(os.Stdin)
	go func() { //here starts the second goroutine which listens commands from console
		for scanner.Scan() {
			line := scanner.Text() //when write command and enter, string puts into "line" variable
			commandChan <- line    //added to channel
			if line == "exit" {
				close(commandChan)
				break
			}
		}
	}()

	// Handler for console commands
	client := &http.Client{}

	for cmd := range commandChan { // handler handles 5 types of commands
		action := -1
		if strings.Contains(cmd, "new sla") {
			action = 1
		}
		if strings.Contains(cmd, "send msg") {
			action = 2
		}
		if strings.Contains(cmd, "send cor msg") {
			action = 3
		}
		if strings.Contains(cmd, "del sla") {
			action = 4
		}
		if strings.Contains(cmd, "exit") {
			action = 5
		}

		switch action {
		case 1: //try to get slaid from console as a third word
			if len(strings.Split(cmd, " ")) < 3 {
				fmt.Println("You should enter SLA ID")
				break
			}
			slaId := strings.Split(cmd, " ")[2]
			reqUrl := config.AdaptorUrl + "/adaptor/sla-creation/" + slaId //generate URI request to translator.go with this ID
			fmt.Println("Action: PUT", reqUrl)

			req, err := http.NewRequest("PUT", reqUrl, nil)
			if err != nil {
				log.Fatal(err)
			}

			resp, err := client.Do(req)
			if err != nil {
				log.Fatal(err)
			} else {
				defer resp.Body.Close()
				fmt.Println("Response Status Code:", resp.StatusCode)
			}

		case 2, 3: // for commands "send msg" and "send cor msg"
			// Create fake messages
			var testMessage []byte
			if action == 2 { //create fake json object
				labels := strings.Split(cmd, " ")
				text := ` 
					{
						"component": "specs-counter-filter",
						"object": "queue",
						"labels": [
							`

				for i := 2; i < len(labels); i++ {
                    if i>2 {
                        text += ", "
                    }
					text += `"` + labels[i] + `"`
				}

				text += `   ],
						"type": "integer",
						"data": "3",
						"timestamp": 1451040172,
						"token": {
							"uuid": "specs-counter-filter",
							"seq": 123321
						}
					}
				`
				testMessage = []byte(text)
			} else {
				testMessage = []byte(`{"field": bad syntax}`) //create corrupted JSON object
			}

			reqUrl := config.AdaptorUrl + "/adaptor/events"
			fmt.Println("Action: PUT", reqUrl)

			req, err := http.NewRequest("PUT", reqUrl, bytes.NewBuffer(testMessage))
			if err != nil {
				log.Fatal(err)
			}
			req.Header.Set("Content-Type", "application/json")

			resp, err := client.Do(req)
			if err != nil {
				log.Fatal(err)
			} else {
				defer resp.Body.Close()
				fmt.Println("Response Status Code:", resp.StatusCode)
			}

		case 4: //Parse sla id from command line and generate request with this ID to /adaptor/sla-termination/
			if len(strings.Split(cmd, " ")) < 3 {
				fmt.Println("You should enter SLA ID")
				break
			}
			slaId := strings.Split(cmd, " ")[2]
			reqUrl := config.AdaptorUrl + "/adaptor/sla-termination/" + slaId
			fmt.Println("Action: PUT", reqUrl)

			req, err := http.NewRequest("PUT", reqUrl, nil)
			if err != nil {
				log.Fatal(err)
			}

			resp, err := client.Do(req)
			if err != nil {
				log.Fatal(err)
			} else {
				defer resp.Body.Close()
				fmt.Println("Response Status Code:", resp.StatusCode)
			}
		case 5:
			fmt.Println("Action: Exit programm")
		case -1:
			fmt.Println("Bad command syntax. Try again")
		}

		fmt.Print("\nEnter command: ")
	}
}
