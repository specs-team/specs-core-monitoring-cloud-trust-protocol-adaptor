package main

import (
	"./adaptor"
	"./configure"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	//
	"github.com/cloudsecurityalliance/ctpd/server"
	"github.com/cloudsecurityalliance/ctpd/server/ctp"
	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

/*
 * The ugly globals, for the local database storage and the configuration.
 */
var mongoSession *mgo.Session
var config = configure.Create()
var _BuildId_ string

type SpecsMeasurementTranslation struct {
	Key   string   `bson:"_id"`
	SlaId string   `bson:"slaId"`
	Link  ctp.Link `bson:"link"`
}

// helper function for error reporting
func httpErrorWithLog(w http.ResponseWriter, status int, message string, err error) {
	if message == "" {
		if err != nil {
			message = err.Error()
		}
	} else {
		if err != nil {
			message += " (" + err.Error() + ")"
		}
	}
	log.Printf("Http ERROR %d: %s", status, message)
	http.Error(w, message, status)
}

func httpOKWithLog(w http.ResponseWriter, status int) {
	log.Println("Http OK 200 was returned")
	w.WriteHeader(status)
}

// Helper function to check if the sla identified by slaId already exists
func SlaExists(slaId string) (bool, error) {
	client := adaptor.NewCtpClient(config["ctp-server-endpoint"], config["ctp-auth-token"])

	_, err := client.FindResourceByName("@/serviceViews", slaId)
	if err == adaptor.ResourceNotFound {
		return false, nil
	}
	if err == nil {
		return true, nil
	}
	return false, err
}

// Do the processing of the SLA and CTP calls
func ProcessSla(slaId string, sla *adaptor.SpecsSla) error {
	client := adaptor.NewCtpClient(config["ctp-server-endpoint"], config["ctp-auth-token"])

	log.Println("Processing METRICS in SLA " + slaId)
	metrics := sla.CtpMetrics()

	if len(metrics) == 0 {
		return fmt.Errorf("No metric found in SLA %s.", slaId)
	}

	for _, m := range metrics {
		if metric_uri, err := client.FindResourceByName("@/metrics", m.Name); err != nil {
			if err != adaptor.ResourceNotFound {
				return err
			}
			if err = client.PostResource("@/metrics", m, m); err != nil {
				return err
			}
		} else {
			log.Println("Found metric " + m.Name + " at " + metric_uri)
			if err = client.GetResource(metric_uri, m); err != nil {
				return err
			}
		}
	}

	log.Println("Processing ACCOUNTS in SLA " + slaId)
	account := sla.CtpAccount()

	if account == nil {
		return fmt.Errorf("No customer found in SLA %s.", slaId)
	}

	if _, err := client.FindResourceByName("@/accounts", sla.Context.AgreementInitiator); err != nil {
		if err != adaptor.ResourceNotFound {
			return err
		}
		if err = client.PostResource("@/accounts", account, account); err != nil {
			return err
		}
	} else {
		log.Println("Found account " + account.Name)
	}

	log.Println("Processing SERVICE VIEWS in SLA " + slaId)
	sv := sla.CtpServiceView(slaId)

	if sv == nil {
		return fmt.Errorf("No services found in SLA %s.", slaId)
	}

	if _, err := client.FindResourceByName("@/serviceViews", slaId); err != adaptor.ResourceNotFound {
		return fmt.Errorf("Service view for sla %s already exists", slaId)
	}
	sv.AccessTags = ctp.NewTags("account:" + account.Id.String())
	if err := client.PostResource("@/serviceViews", sv, sv); err != nil {
		return err
	}

	log.Println("Processing ASSET in SLA " + slaId)
	asset := sla.CtpAsset()

	if asset == nil {
		return fmt.Errorf("No assets found in SLA %s.", slaId)
	}

	if err := client.PostResource(string(sv.Assets), asset, asset); err != nil {
		return err
	}

	attributes := sla.CtpAttributes()
	for _, attribute := range attributes {
		log.Printf("Processing ATTRIBUTE %s in SLA %s", attribute.Name, slaId)
		if err := client.PostResource(string(asset.Attributes), attribute, attribute); err != nil {
			return err
		}

		measurements := sla.CtpMeasurements(slaId, attribute, metrics)
		for _, measurement := range measurements {
			log.Printf("Processing MEASUREMENT %s in SLA %s", measurement.Name, slaId)
			if err := client.PostResource(string(attribute.Measurements), measurement, measurement); err != nil {
				return err
			}

			err := mongoSession.DB("translator").C("measurements").Insert(SpecsMeasurementTranslation{measurement.Name, slaId, measurement.Self})
			if err != nil {
				return err
			}
		}
	}
	log.Println("Done with SLA " + slaId)
	return nil
}

type SpecsMeasurement struct {
	Result interface{} `json:"result"`
}

type MeasurementResult struct {
	Result *server.Result `json:"result"`
}

// Do the processing of the json events
func ProcessEvent(event *adaptor.SpecsEvent) (string, error) {
	var measurementResult MeasurementResult
	var measurement server.Measurement
	const metric_prefix = "security_metric_"
	const sla_id_prefix = "sla_id_"

	if len(event.Labels) < 2 {
		return "", fmt.Errorf("Event does not have enough labels")
	}

	var sla_id string
	var sla_measurement string

	for _, v := range event.Labels[0:] {
		if strings.HasPrefix(v, metric_prefix) {
			sla_measurement = strings.TrimPrefix(v, metric_prefix)
		} else if strings.HasPrefix(v, sla_id_prefix) {
			sla_id = strings.TrimPrefix(v, sla_id_prefix)
		}
	}
	if sla_measurement == "" {
		return "", fmt.Errorf("Event labels do not contain a reference to a metric")
	}

	if sla_id == "" {
		return "", fmt.Errorf("Event labels do not contain a reference to an sla")
	}

	measurementResult.Result = new(server.Result)
	// measurementResult.Result.Value = []server.ResultRow{server.ResultRow{"result": event.Data}}
	measurementResult.Result.UpdateTime = ctp.Timestamp(event.Timestamp)

	switch event.Type {
	case "integer":
		intval, err := strconv.Atoi(event.Data)
		if err != nil {
			return "", fmt.Errorf("Event integer data is in an incorrect format")
		}
		measurementResult.Result.Value = []server.ResultRow{server.ResultRow{"result": intval}}

	case "boolean":
		if event.Data == "true" {
			measurementResult.Result.Value = []server.ResultRow{server.ResultRow{"result": true}}
		} else if event.Data == "false" {
			measurementResult.Result.Value = []server.ResultRow{server.ResultRow{"result": false}}
		} else {
			return "", fmt.Errorf("Event boolean data must be true or false")
		}

	case "string":
		measurementResult.Result.Value = []server.ResultRow{server.ResultRow{"result": event.Data}}

	default:
		return "", fmt.Errorf("Event type must be integer, boolean or string, got '%s'", event.Type)
	}

	client := adaptor.NewCtpClient(config["ctp-server-endpoint"], config["ctp-auth-token"])

	var measurementTranslation SpecsMeasurementTranslation

	err := mongoSession.DB("translator").C("measurements").FindId(sla_id + ":" + sla_measurement).One(&measurementTranslation)
	if err != nil {
		return "", fmt.Errorf("The SLA %s does not exist or does not contain a measurement based on metric '%s'", sla_id, sla_measurement)
	}

	if err := client.PutResource(string(measurementTranslation.Link)+"?x=result", &measurementResult, &measurement); err != nil {
		return "", fmt.Errorf("Could not update measurement (error: %s)", err.Error())
	}

	return sla_id, nil
}

// Handler for PUT request with new SLA ID
func NewSlaHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// STEP 1:

	log.Printf("Action: PUT request from server %s with new SLA ID", r.RemoteAddr)

	// Checking for syntax error in request
	//if _, err := strconv.Atoi(ps.ByName("sla_id")); err != nil {
	//	httpErrorWithLog(w, 400, "Bad SLA ID syntax", err)
	//	return
	//}

	slaId := ps.ByName("sla_id")

	// Check if SLA is already registered.
	present, err := SlaExists(slaId)
	if err != nil {
		httpErrorWithLog(w, 500, "Could not check if the SLA already exists", err)
		return
	}
	if present {
		httpErrorWithLog(w, 409, "An SLA with the same ID is already registered", nil)
		return
	}

    // REMOVED:
	//If there are no errors we should send 200 "OK" to server. Here Step 1 is finished and at this point we have SLA ID in slaid variable.
    //	httpOKWithLog(w, 200)
    // /REMOVED

	// STEP 2:

	// Query the Server API about the content of the SLA. Step 2 starts.
	client := &http.Client{}
	reqUrl := config["specs-endpoint"] + slaId //generate url request.
	log.Println("Action: Query the Server", reqUrl)
	req, err := http.NewRequest("GET", reqUrl, nil)
	if err != nil {
        httpErrorWithLog(w, 500, "Failed to create query " + reqUrl, err)
		return
	}

	resp, err := client.Do(req)
	if err != nil {
        httpErrorWithLog(w, 500, "Failed to perform query " + reqUrl, err)
		return
	} else {
		// Handle request with XML content
		defer resp.Body.Close()
		log.Printf("Action: Reading XML from %s", reqUrl)
		xmlContent, err := ioutil.ReadAll(resp.Body) //If everything works, then we can save server's xml file into xmlContent variable
		if err != nil {
            httpErrorWithLog(w, 500, "Failed to read XML data from " + reqUrl, err)
			return
		}

		var sla adaptor.SpecsSla
		if err := xml.Unmarshal(xmlContent, &sla); err != nil { //Unmarshal function from encoding/xml package
            httpErrorWithLog(w, 500, "Failed to parse XML data from " + reqUrl, err)
			return
		}

		if err := ProcessSla(slaId, &sla); err != nil {
			httpErrorWithLog(w, 500, "Internal error in ProcessSla for " + reqUrl, err)
			return
		}
	}
	log.Printf("Successfully created SLA %s from %s",slaId,reqUrl)
    httpOKWithLog(w, 200)
}

// Handler for PUT request with event JSON
func EventHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) { //Due to documentation, server sends JSON file in the body
    var sla_id string
	log.Printf("Action: PUT request from %s with JSON event", r.RemoteAddr)

	body, err := ioutil.ReadAll(r.Body) //save binary data to body variable
	if err != nil {
		httpErrorWithLog(w, 400, "Can't read JSON event from server", nil)
		return
	}

	var newMessage adaptor.SpecsEvent
	if err := json.Unmarshal(body, &newMessage); err != nil { //Here we try to Unmarshal data to "message" variable with type Message
		httpErrorWithLog(w, 400, "JSON object is incorrect", err)
		log.Printf("Offending JSON message: %s", body)
		return
	}

	if sla_id, err = ProcessEvent(&newMessage); err != nil {
		log.Printf("ERROR in ProcessEvent: %s", err.Error())
		http.Error(w, err.Error(), 400)
		return
	}

    if newMessage.Token.Uuid=="" {
        newMessage.Token.Uuid="(unspecificied uuid)"
    }

    log.Printf("SLA %s: Successfully processed event %s:%d from component %s", sla_id, newMessage.Token.Uuid, newMessage.Token.Seq, newMessage.Component)
}

// Handler for PUT request for SLA termination
func TerminateSlaHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) { //
	log.Printf("Action: PUT request from server %s for SLA termination", r.RemoteAddr)

	// Checking for syntax error in request. Same as in NewSlaHandler.
	if _, err := strconv.Atoi(ps.ByName("sla_id")); err != nil { //If SLA id is integer, we try to find this SLA id in our database. If it exists, we move to line 175 and delete SLA, otherwise send error 404 to server.
		httpErrorWithLog(w, 400, "Bad SLA ID syntax", err)
		return
	} // here finishes step 4

	slaId := ps.ByName("sla_id")

	client := adaptor.NewCtpClient(config["ctp-server-endpoint"], config["ctp-auth-token"])

	// Check if SLA exists
	link, err := client.FindResourceByName("@/serviceViews", slaId)
	if err == adaptor.ResourceNotFound {
		httpErrorWithLog(w, 404, "Requested SLA doesn't exist", err)
		return
	}

	// Try to delete the SLA on the CTP server
	if err := client.DeleteResource(link); err != nil {
		httpErrorWithLog(w, 500, "Could not delete SLA "+slaId, err)
		return
	}

	// Remove from local database
	change, err := mongoSession.DB("translator").C("measurements").RemoveAll(bson.M{"slaId": slaId})
	if err != nil {
		log.Printf("Failed to remove measurement from local database for SLA %s: %s", slaId, err.Error())
		return
	}
	log.Printf("%d entries where removed in local database", change.Removed)

	log.Println("Successfully deleted SLA " + slaId)
}

func init() {
	config["listen"] = "localhost:8000"
	config["specs-endpoint"] = "http://localhost:9000/cloud-sla/slas/" //We change this into the real url. For now, it's the server-imitator address
	config["mongodb"] = "localhost"
	config["ctp-server-endpoint"] = "http://localhost:8080/api/1.0/"
	config["ctp-auth-token"] = "0000"
}

func main() {
	var err error
	var baseUri server.BaseURI

	if _BuildId_ == "" {
		_BuildId_ = `**not set, use -ldflags "-X main._BuildId_ <id>" as a compile option to set <id>**`
	}

	conf_filename, err := configure.SearchDefaultLocations("specs-adaptor.conf")
	if err != nil {
		log.Printf("Could not find a 'specs-adaptor.conf' configuration file, using defaults.")
	} else {
		if err = config.ReadFile(conf_filename); err != nil {
			log.Fatal(err)
		}
	}

	if config["log-file"] != "" {
		file, err := os.Create(config["log-file"])
		if err != nil {
			log.Fatalf("Could not open log file '%s': %s.", config["log-file"], err.Error())
		}
		defer file.Close()
		log.Printf("All following log messages will be written to '%s', please check that file for futher information.", config["log-file"])
		log.SetOutput(file)
	}

	log.Printf("Starting ctp-specs adaptor (build %s).\n", _BuildId_)

	client := adaptor.NewCtpClient(config["ctp-server-endpoint"], config["ctp-auth-token"])
	if err := client.GetResource("@/", &baseUri); err != nil {
		log.Fatalf("Could not connect to CTP server %s: %s", config["ctp-server-endpoint"], err.Error())
	}
	log.Printf("CTP server is up at %s", baseUri.Self)

	_, err = http.Get(config["specs-endpoint"])
	if err != nil {
        log.Printf("Failed test connection to specs endpoint '%s': %s", config["specs-endpoint"], err.Error())
        log.Printf("*** WARNING: The specs endpoint does not seem to be responding. The CTP adaptor may not work correctly. ***")
	} else {
	    log.Printf("Successfully tested connection to specs endpoint '%s'", config["specs-endpoint"])
    }

	mongoSession, err = mgo.Dial(config["momgodb"])
	if err != nil {
		log.Fatal(err)
	}
	defer mongoSession.Close()

	bi, err := mongoSession.BuildInfo()
	if err != nil {
		log.Fatal("Failed to retrieve version info for database %s: %s", config["mongodb"], err.Error())
	}
	log.Printf("Database %s is running mongodb version %s (%s)", config["mongodb"], bi.Version, bi.SysInfo)

	// configure the handlers
	router := httprouter.New()
	router.PUT("/adaptor/sla-creation/:sla_id", NewSlaHandler)          //handler for step1: reception of sla_id
	router.PUT("/adaptor/events", EventHandler)                         //here starts step 3
	router.PUT("/adaptor/sla-termination/:sla_id", TerminateSlaHandler) //handler function for step 4

	log.Printf("Starting adaptor on %s", config["listen"])
	err = http.ListenAndServe(config["listen"], router)
	if err != nil {
		log.Println(err)
	}
}
