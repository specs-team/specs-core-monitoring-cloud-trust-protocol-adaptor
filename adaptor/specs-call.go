package adaptor

// This file contains utility functions to make http calls to the CTP server

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
    "github.com/cloudsecurityalliance/ctpd/server"
	"github.com/cloudsecurityalliance/ctpd/server/ctp"
	"net/http"
	"net/url"
)

var ResourceNotFound = errors.New("Ressource not found")

type CtpClient struct {
	baseUri ctp.Link
	token   string
}

func NewCtpClient(baseUri string, token string) *CtpClient {
	return &CtpClient{ctp.Link(baseUri), token}
}

func (client *CtpClient) ParseId(response interface{}) bool {
	res, ok := response.(*ctp.Resource)
	if ok {
		parts, ok2 := ctp.ParseLink(client.baseUri, "@/$/$", res.Self)
		if ok2 {
			res.Id = ctp.Base64Id(parts[1])
			return true
		}
	}
	return false
}

func (client *CtpClient) GetResource(uri string, response interface{}) error {
	uri_link := string(ctp.NewLink(client.baseUri, uri))

	req, err := http.NewRequest("GET", uri_link, nil)
	if err != nil {
		return err
	}
	req.Header.Set("Authorization", "Bearer "+client.token)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	switch resp.StatusCode {
	case 200:
		// OK
	case 404:
		return ResourceNotFound
	default:
		return errors.New("Expected status 200, got " + resp.Status)
	}

	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(response); err != nil {
		return err
	}
	return nil
}

func (client *CtpClient) PostResource(uri string, request interface{}, response interface{}) error {
	json_body, err := json.Marshal(request)
	if err != nil {
		return (err)
	}
	body := bytes.NewBuffer(json_body)

	uri_link := string(ctp.NewLink(client.baseUri, uri))

	req, err := http.NewRequest("POST", uri_link, body)
	if err != nil {
		return err
	}
	req.Header.Set("Authorization", "Bearer "+client.token)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != 201 {
		return errors.New("Expected status 201, got " + resp.Status)
	}

	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(response); err != nil {
		return err
	}
	return nil
}

func (client *CtpClient) PutResource(uri string, request interface{}, response interface{}) error {
	json_body, err := json.Marshal(request)
	if err != nil {
		return (err)
	}
	body := bytes.NewBuffer(json_body)

	uri_link := string(ctp.NewLink(client.baseUri, uri))

	req, err := http.NewRequest("PUT", uri_link, body)
	if err != nil {
		return err
	}
	req.Header.Set("Authorization", "Bearer "+client.token)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		return fmt.Errorf("Expected status 200, got %s for %s", resp.Status, uri_link)
	}

	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(response); err != nil {
		return err
	}
	return nil
}

func (client *CtpClient) DeleteResource(uri string) error {
	uri_link := string(ctp.NewLink(client.baseUri, uri))

	req, err := http.NewRequest("DELETE", uri_link, nil)
	if err != nil {
		return err
	}
	req.Header.Set("Authorization", "Bearer "+client.token)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != 204 {
		return fmt.Errorf("Expected status 204, got %s for %s", resp.Status, uri_link)
	}
	return nil
}

func (client *CtpClient) FindResourceByName(uri string, name string) (string, error) {
	var col server.Collection

	if err := client.GetResource(uri+"?name="+url.QueryEscape(name), &col); err != nil {
		return "", err
	}
	if len(col.Items) == 0 {
		return "", ResourceNotFound
	}
	if len(col.Items) > 1 {
		return "", fmt.Errorf("Expected 1 element, got %d", len(col.Items))
	}
	return string(col.Items[0].Link), nil
}
