package adaptor

// This file contains utility function to parse SLAs in XML and the declaration of JSON event

import (
	"encoding/xml"
	"github.com/cloudsecurityalliance/ctpd/server"
	"github.com/cloudsecurityalliance/ctpd/server/ctp"
)

/*

Note:
    The XML marshaler in GO expects all structs to start with an upercase
    letter (e.g. 'Terms' not 'terms'). But, by default, Go also uses the name
    of the struct to identify the xml element it maps to (e.g. <Term>...</Term>
    maps to struct Term).  So this mechanism does not work well with elements
    that start with a lower case (e.g. <serviceResources>...) To fix this we
    annotate the structure with an xml annotatation that gives the correct
    spelling of the element (e.g.  `xml:"serviceResources"`).

    see: https://golang.org/pkg/encoding/xml/#Unmarshal
*/

type SpecsSloExpression struct {
	OneOpExpression *struct {
		Operator string `xml:"operator,attr"`
		Operand  string `xml:"operand,attr"`
	} `xml:"oneOpExpression"`
	TwoOpExpression *struct {
		Operator string `xml:"operator,attr"`
		Operand1 string `xml:"operand1,attr"`
		Operand2 string `xml:"operand2,attr"`
	} `xml:"twoOpExpression"`
}

type SpecsSla struct {
	Name    string
	Context struct {
		AgreementInitiator string
		AgreementResponder string
	}
	Terms struct {
		XMLName xml.Name
		All     struct {
			ServiceDescriptionTerm struct {
				Name               string `xml:"Name,attr"`
				ServiceName        string `xml:"ServiceName,attr"`
				ServiceDescription struct {
					ServiceResources struct {
						ResourcesProvider []struct {
							VM []struct {
								Appliance   string `xml:"appliance,attr"`
								Description string `xml:"description,attr"`
							} `xml:"VM"`
						} `xml:"resourcesProvider"`
					} `xml:"serviceResources"`
					Capabilities struct {
					} // ignored
					SecurityMetrics struct {
						Metric []struct {
							Name             string `xml:"name,attr"`
							ReferenceId      string `xml:"referenceId,attr"`
							MetricDefinition struct {
								Unit struct {
									Name         string `xml:"name,attr"`
									IntervalUnit *struct {
										IntervalItemsType string `xml:"intervalItemsType"`
										IntervalItemStart string `xml:"intervalItemStart"`
										IntervalItemStop  string `xml:"intervalItemStop"`
										IntervalItemStep  string `xml:"intervalItemStep"`
									} `xml:"intervalUnit"`
									EnumUnit *struct {
										// todo
									} `xml:"enumUnit"`
								} `xml:"unit"`
								Expression string `xml:"expression"`
							} `xml:"MetricDefinition"`
						} `xml:"Metric"`
					} `xml:"security_metrics"`
				} `xml:"serviceDescription"`
			}
			ServiceReference struct {
				Name        string `xml:"Name,attr"`
				ServiceName string `xml:"ServiceName,attr"`
			} `xml:"ServiceReference"`
			ServiceProperties struct {
				VariableSet struct {
					Variable []struct {
						Name   string `xml:"Name,attr"`
						Metric string `xml:"Metric,attr"`
					} `xml:Variable`
				} `xml:"VariableSet"`
			} `xml:"ServiceProperties"`
			GuaranteedTerm struct {
				ServiceLevelObjective struct {
					CustomServiceLevel struct {
						ObjectiveList struct {
							SLO []struct {
								SLO_ID        string `xml:"SLO_ID,attr"`
								MetricREF     string `xml:"MetricREF"`
								SLOexpression SpecsSloExpression
							} `xml:"SLO"`
						} `xml:"objectiveList"`
					} `xml:"CustomServiceLevel"`
				} `xml:"ServiceLevelObjective"`
			} `xml:"GuaranteeTerm"`
		}
	}
}

type SpecsEvent struct {
	Component string   `json:"component"`
	Data      string   `json:"data"`
	Type      string   `json:"type"`
	Object    string   `json:"object"`
	Labels    []string `json:"labels"`
	Timestamp float64  `json:"timestamp"`
	Token     struct {
		Uuid string `json:"uuid"`
		Seq  int    `json:"seq"`
	} `json:"token"`
}

func (s *SpecsSla) CtpAccount() *server.Account {
	if s.Context.AgreementInitiator == "" {
		return nil
	}

	var acc *server.Account = new(server.Account)
	acc.Name = s.Context.AgreementInitiator
	acc.AccessTags = ctp.NewTags("role:admin")
	acc.AccountTags = ctp.NewTags("role:user")
	return acc
}

func (s *SpecsSla) CtpServiceView(slaId string) *server.ServiceView {
	if s.Context.AgreementResponder == "" {
		return nil
	}

	var sv *server.ServiceView = new(server.ServiceView)
	sv.Name = slaId
	sv.Annotation = "Based on " + s.Name
	sv.Provider = s.Context.AgreementResponder
	return sv
}

func (s *SpecsSla) CtpAsset() *server.Asset {
	if s.Terms.All.ServiceDescriptionTerm.ServiceName == "" {
		return nil
	}

	var a *server.Asset = new(server.Asset)
	a.Name = s.Terms.All.ServiceDescriptionTerm.ServiceName
	a.Annotation = s.Terms.All.ServiceDescriptionTerm.Name
	return a
}

func (s *SpecsSla) CtpMetrics() []*server.Metric {
	var metrics []*server.Metric

	for _, e := range s.Terms.All.ServiceDescriptionTerm.ServiceDescription.SecurityMetrics.Metric {
		metric := new(server.Metric)
		metric.Name = e.ReferenceId
		metric.Annotation = e.MetricDefinition.Expression
		if e.MetricDefinition.Unit.IntervalUnit != nil {
			var format server.ResultColumnFormat
			format.Name = "result"
			switch e.MetricDefinition.Unit.IntervalUnit.IntervalItemsType {
			case "integer":
				format.Type = "number"
			case "float":
				format.Type = "number"
			case "boolean":
				format.Type = "boolean"
			default:
				format.Type = "string"
			}
			metric.ResultFormat = append(metric.ResultFormat, format)
		}
		metrics = append(metrics, metric)
	}
	return metrics
}

func (s *SpecsSla) CtpAttributes() []*server.Attribute {
	var attrs []*server.Attribute

	for _, e := range s.Terms.All.ServiceProperties.VariableSet.Variable {
		attr := new(server.Attribute)
		attr.Name = e.Name
		attrs = append(attrs, attr)
	}
	return attrs
}

func CtpObjective(slo *SpecsSloExpression) *server.Objective {
	objective := new(server.Objective)

	switch {
	case slo.OneOpExpression != nil:
		switch slo.OneOpExpression.Operator {
		case "equal":
			objective.Condition = "value[0].result==" + slo.OneOpExpression.Operand
		case "greater then":
			objective.Condition = "value[0].result>" + slo.OneOpExpression.Operand
		case "less then":
			objective.Condition = "value[0].result<" + slo.OneOpExpression.Operand
		case "greater then or equal":
			objective.Condition = "value[0].result>=" + slo.OneOpExpression.Operand
		case "less then or equal":
			objective.Condition = "value[0].result<=" + slo.OneOpExpression.Operand
		default:
			// todo error log
			return nil
		}
	case slo.TwoOpExpression != nil:
		switch slo.TwoOpExpression.Operator {
		case "in excluded":
			objective.Condition = "value[0].result>" + slo.TwoOpExpression.Operand1
			objective.Condition += "&& value[0].result<" + slo.TwoOpExpression.Operand2
		case "in included":
			objective.Condition = "value[0].result>=" + slo.TwoOpExpression.Operand1
			objective.Condition += "&& value[0].result<=" + slo.TwoOpExpression.Operand2
		default:
			// todo error log
			return nil
		}
	default:
		// todo error
		return nil
	}
	return objective
}

func (s *SpecsSla) CtpMeasurements(slaId string, attribute *server.Attribute, metrics []*server.Metric) []*server.Measurement {
	var measurements []*server.Measurement

	metricTable := make(map[string]*server.Metric)
	for _, varname := range s.Terms.All.ServiceProperties.VariableSet.Variable {
		for _, metric := range metrics {
			if varname.Metric == metric.Name {
				metricTable[varname.Name] = metric
			}
		}
	}

	for _, e := range s.Terms.All.GuaranteedTerm.ServiceLevelObjective.CustomServiceLevel.ObjectiveList.SLO {
		if e.MetricREF == attribute.Name {
			measurement := new(server.Measurement)

			measurement.Self = ctp.Link(string(attribute.Self) + "/measurements")
			if metricp, ok := metricTable[e.MetricREF]; ok {
				measurement.Metric = metricp.Self
				measurement.Name = slaId + ":" + metricp.Name
			} else {
				// todo return error
				return nil
			}
			measurement.Objective = CtpObjective(&e.SLOexpression)
			measurement.State = "pending"
			measurements = append(measurements, measurement)
		}
	}
	return measurements
}
