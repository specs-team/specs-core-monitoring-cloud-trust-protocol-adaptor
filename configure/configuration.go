package configure

import (
	"bufio"
	"fmt"
	"os"
	"os/user"
	"path"
	"regexp"
	"strings"
)

type Configuration map[string]string

var validEntry1 = regexp.MustCompile(`^([-a-zA-Z0-9_]+)\s*=\s*([^ "\t\r\n]+)$`)
var validEntry2 = regexp.MustCompile(`^([-a-zA-Z0-9_]+)\s*=\s*"([^"]*)"$`)

func Create() Configuration {
	return make(Configuration)
}

/* Read a configuration file 'filename' 
 *
 * The file must have entries of the form 
 *      key=value
 * or 
 *      key="value"
 * 
 * Comments start with '#' and are ignored
 *
 * BUG: a value cannot contain a '#' character. A more sophisticated parser is needed.
 */
func (conf Configuration) ReadFile(filename string) error {
	info, err := os.Stat(filename)
	if err != nil {
		return err
	}
	if (info.Mode() & 077) != 0 {
		return fmt.Errorf("Permissions 0%o for configuration file '%s' are too open.", info.Mode()&os.ModePerm, filename)
	}

	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	linecount := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		linecount++
		if i := strings.IndexByte(line, '#'); i >= 0 {
			line = line[0:i]
		}
		line = strings.TrimSpace(line)
		if len(line) > 0 {
			if r := validEntry1.FindStringSubmatch(line); r != nil {
				conf[r[1]] = r[2]
			} else if r := validEntry2.FindStringSubmatch(line); r != nil {
				conf[r[1]] = r[2]
			} else {
				return fmt.Errorf("Error on line %d in configuration file '%s'", linecount, filename)
			}
		}
	}
	return nil
}

/* Given {basename}, find the first file that exists in the following order:
 *      ./{basename}
 *      $HOME/.{basename}
 *      /etc/{basename}
 * 
 * If no file exists, return an error.
 */
func SearchDefaultLocations(basename string) (string, error) {
	var fname string

	cwd, err := os.Getwd()
	if err == nil {
		fname = path.Join(cwd, basename)
		if _, r := os.Stat(fname); r == nil {
			return fname, nil
		}
	}

	usr, err := user.Current()
	if err == nil {
		fname = path.Join(usr.HomeDir, "."+basename)
		if _, r := os.Stat(fname); r == nil {
			return fname, nil
		}
	}

	fname = path.Join("/etc/", basename)
	if _, r := os.Stat(fname); r != nil {
		return "", r
	}
	return fname, nil
}
