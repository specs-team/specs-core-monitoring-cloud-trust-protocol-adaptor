# SPECS-CTP Adaptor #

This repository contains the SPECS-CTP adaptor, which acts as an interface between the SPECS platform and the CTP server (which is described in another repository). 

This code is still preliminary.

### Compiling ###

The code relies on part of the ctp server code, which must be present. This can be assured with the following command:

    go get github.com/cloudsecurityalliance/ctpd

Compilation can be executed with 

    go build specs-ctp-adaptor.go

### Running ###

The program will search for the first configuration file it finds in the following directories:

1. the file `specs-adaptor.conf` the current working directory.
2. the file `.specs-ctp-adaptor.conf` in `$HOME`.
3. the file `/etc/specs-ctp-adaptor.conf`.

The file must be set to be readable only by the user currently running the process.

This configuration file should be customised to the platform characteristics:

```cfg
# listen describes the base address where the adaptor will listen for connections from SPECS
# This base address will be completed to form the 3 URL endpoints provided to SPECS 
# (e.g. http://localhost:8000/adaptor/events) 
listen = "localhost:8000"

# specs-endpoint describes the SLA management endpoint.
specs-endpoint = "http://localhost:9000/cloud-sla/slas/"

# mongodb describes the address (and optional port, user, password) of the database
# used by the adaptor to cache some information.
mongodb = "localhost"

# ctp-server-endpoint describes the CTP server endpoint
ctp-server-endpoint = "http://localhost:8080/api/1.0/"

# ctp-auth-token describes the authentication token needed to access CTP.
ctp-auth-token = "0000"

# log-file specifies a file to which logs will be written intead of 
# the standard output (default).
# example:
# log-file="/tmp/adaptor.log"

```


